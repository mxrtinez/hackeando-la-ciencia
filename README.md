# Hackeando la Ciencia

Jupyter: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/mxrtinez%2Fhackeando-la-ciencia/binder)
Jupyter + R: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/mxrtinez%2Fhackeando-la-ciencia/binder?filepath=InspeccionDatos_TallerOAW_SCiAC.ipynb)
RStudio: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/mxrtinez%2Fhackeando-la-ciencia/binder?urlpath=rstudio)


Taller de Entrenamiento: Reproducibilidad en ciencias como integradora entre Datos Abiertos, Educación Abierta y Ciencia Abierta